module Main (main) where

import Control.Monad (when, liftM)
import System.Exit (exitSuccess)
import System.Environment (getArgs)
import System.Console.GetOpt

import Hplayer.Hplayer

data Flag = Help
          | PlaylistFile FilePath
          | Version
          deriving (Show, Eq)

options :: [OptDescr Flag]
options = [
    Option "p" ["playlist"]
        (ReqArg PlaylistFile "PLAYLIST")
        "reads playlist file for song selection",
    Option "h" ["help"]
        (NoArg Help)
        "display this message",
    Option "v" ["version"]
        (NoArg Version)
        "Print version"
        ]

printHelp :: IO ()
printHelp = putStrLn $ unlines [
    "NAME",
    "\tHplayer - A haskell command line wrapper to mplayer",
    "",
    "",
    "SYNOPSIS",
    "\tHplayer [song ..]",
    "\tHplayer -p playlist",
    "\tHplayer -h",
    "\tHplayer -v",
    "",
    "",
    "DESCRIPTION",
    "\tHplayer is a simple command line wrapper to mplayer written in haskell",
    "",
    "",
    usageInfo "USAGE" options]

printVersion :: IO ()
printVersion = putStrLn "Version 1.0"

-- maybe move this into Hplayer module?
parsePlaylist :: Flag -> IO [FilePath]
parsePlaylist (PlaylistFile f) = liftM lines $ readFile f -- read a file and make a list out of lines
parsePlaylist _                = return [""]

main :: IO ()
main =  do
    rawArgs <- getArgs
    (flags, args, _) <- return $ getOpt RequireOrder options rawArgs

    when (Help `elem` flags) $ do
        printHelp
        exitSuccess

    when (Version `elem` flags) $ do
        printVersion
        exitSuccess

    -- get and parse the playlist file given (if any)
    -- if none are given, the list will be populated with a list of ""
    playlist <- mapM parsePlaylist flags

    let notEmpty = not . null

    if notEmpty playlist then do -- if a playlist was given
        let p = filter notEmpty $ concat playlist
        hPlayer $ Playlist p -- play it
    else -- otherwise just play the song/s given
        if notEmpty args then
            mapM_ (hPlayer . Song) args
        else
            printHelp
