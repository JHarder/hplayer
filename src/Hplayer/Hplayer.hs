module Hplayer.Hplayer (hPlayer, Argument(..)) where

import System.Directory (doesFileExist)
import System.Process (system)
import Control.Monad (void)

data Argument = Song FilePath
              | Playlist [FilePath]
              deriving Show

hPlayer :: Argument -> IO ()
hPlayer (Song song) = playSong song
hPlayer (Playlist list) = mapM_ playSong list

playSong :: FilePath -> IO ()
playSong song = do
    exists <- doesFileExist song
    if exists then
        -- figure out how to run mplayer process
        -- "Playing file: " ++ song
        void $ system ("mplayer " ++ show song)
    else
        putStrLn $ "Could not find file: " ++ song ++ ", skipping..."
